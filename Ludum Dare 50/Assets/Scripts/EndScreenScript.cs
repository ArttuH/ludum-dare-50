using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EndScreenScript : MonoBehaviour
{
    [SerializeField] private GameObject endScreen;
    [SerializeField] private GameObject timeTrialText;
    [SerializeField] private GameObject enduranceText;

    [SerializeField] private TextMeshProUGUI triviaNumber;
    [SerializeField] private TextMeshProUGUI triviaNumber2;
    [SerializeField] private TextMeshProUGUI triviaNumber3;

    //[SerializeField] private PlayerScript playerScript;

    [SerializeField] AudioSource audioSource;

    private void Awake()
    {
        endScreen.SetActive(false);
        audioSource = GetComponent<AudioSource>();
    }

    public void ShowEndScreen(string elapsedTime)
    {
        TowerManager.instance.towersActive = false;
        audioSource.Play();

        endScreen.SetActive(true);

        triviaNumber.text = elapsedTime;
        triviaNumber2.text = ScoreManager.instance.enemiesDestroyed.ToString();
        triviaNumber3.text = ScoreManager.instance.shotsFired.ToString();
    }

    public void RestartButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("GameScene");
    }

    public void MainMenuButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitButtonPressed()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Application.Quit();
        }
    }
}
