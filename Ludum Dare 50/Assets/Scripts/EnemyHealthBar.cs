using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthBar : MonoBehaviour
{
    [SerializeField] RectTransform rectTransform;

    float initialWidth;

    private void Awake()
    {
        initialWidth = rectTransform.rect.width;
    }

    public void SetHealthBar(float fraction)
    {
        Rect rect = rectTransform.rect;
        rect.width = initialWidth * fraction;

        rectTransform.sizeDelta = new Vector2(rect.width, rect.height);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
