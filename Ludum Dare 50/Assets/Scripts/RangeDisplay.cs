using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeDisplay : MonoBehaviour
{
    public int towerCount;

    HashSet<Transform> towerColliderTransforms;

    private void Awake()
    {
        towerCount = 0;
        towerColliderTransforms = new HashSet<Transform>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("RangeDisplay OnTriggerEnter");

        if (other.CompareTag("Tower"))
        {
            if (!towerColliderTransforms.Contains(other.transform))
            {
                towerColliderTransforms.Add(other.transform);
                ++towerCount;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("RangeDisplay OnTriggerExit");

        if (other.CompareTag("Tower"))
        {
            if (towerColliderTransforms.Contains(other.transform))
            {
                towerColliderTransforms.Remove(other.transform);
                --towerCount;
            }
        }
    }
}
