using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIdleSoundScript : MonoBehaviour
{
    AudioSource audioSource;

    [SerializeField] float minPitch;
    [SerializeField] float maxPitch;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.pitch = Random.Range(minPitch, maxPitch);
        audioSource.Play();
    }
}
