using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIScript : MonoBehaviour
{
    public static UIScript instance;

    [SerializeField] GameObject timeElapsedParent;
    [SerializeField] GameObject resourcesParent;
    [SerializeField] GameObject shopParent;

    [SerializeField] TextMeshProUGUI timeElapsedMinutesValue;
    [SerializeField] TextMeshProUGUI timeElapsedTensValue;
    [SerializeField] TextMeshProUGUI timeElapsedOnesValue;
    [SerializeField] TextMeshProUGUI timeElapsedTenthsValue;
    [SerializeField] TextMeshProUGUI resourcesValue;

    [SerializeField] EndScreenScript endScreenScript;

    string formattedTime;

    private void Awake()
    {
        instance = this;
    }

    public void SetResourcesValue(int resources)
    {
        resourcesValue.text = resources.ToString();
    }

    public void SetTimeElapsedValue(float timeElapsed)
    {
        int minutes = Mathf.FloorToInt(timeElapsed / 60f);
        int seconds = Mathf.FloorToInt(timeElapsed - (minutes * 60f));
        int tens = Mathf.FloorToInt(seconds / 10f);
        int ones = seconds - (tens * 10);
        float spare = (timeElapsed - minutes * 60f - seconds) * 10;

        if (spare >= 9.5f)
        {
            spare = 9.4f;
        }

        //Debug.Log($"spare = {spare}");

        if (timeElapsed > 0f)
        {
            if (minutes > 0)
            {
                formattedTime = $"{minutes.ToString()}:{seconds.ToString("00")}.{spare.ToString("0")}";
            }
            else if (seconds >= 10)
            {
                formattedTime = $"{seconds.ToString("00")}.{spare.ToString("0")}";
            }
            else
            {
                formattedTime = $"{seconds.ToString("0")}.{spare.ToString("0")}";
            }
        }
        else
        {
            formattedTime = "0.0";
            timeElapsedParent.SetActive(false);
            //endScreenScript.ShowEndScreen(towerHeightValue.text);
        }

        //timeLeftValue.text = formattedText;

        timeElapsedMinutesValue.text = minutes.ToString();
        timeElapsedTensValue.text = tens.ToString();
        timeElapsedOnesValue.text = ones.ToString();
        timeElapsedTenthsValue.text = spare.ToString("0");
    }

    public void ShowEndScreen()
    {
        DisableGameUI();
        endScreenScript.ShowEndScreen(formattedTime);
    }

    public void DisableGameUI()
    {
        timeElapsedParent.SetActive(false);
        resourcesParent.SetActive(false);
        shopParent.SetActive(false);
    }

    public void EnableGameUI()
    {
        timeElapsedParent.SetActive(true);
        resourcesParent.SetActive(true);
        shopParent.SetActive(true);
    }
}
