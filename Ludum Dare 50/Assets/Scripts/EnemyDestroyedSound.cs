using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroyedSound : MonoBehaviour
{
    [SerializeField] AudioClip[] audioClips;
    AudioSource audioSource;

    [SerializeField] float minPitch;
    [SerializeField] float maxPitch;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.pitch = Random.Range(minPitch, maxPitch);
        audioSource.PlayOneShot(audioClips[Random.Range(0, audioClips.Length - 1)]);
    }
}
