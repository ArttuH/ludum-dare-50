using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager instance;

    [SerializeField] GameObject enemy1Prefab;
    [SerializeField] GameObject enemy2Prefab;
    [SerializeField] GameObject enemy3Prefab;

    public List<Enemy> enemies;

    public RectTransform canvasRect;
    public RectTransform hudTransform;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        canvasRect = (UIScript.instance.transform.GetComponent<RectTransform>());
        hudTransform = (UIScript.instance.transform.GetChild(0).GetComponent<RectTransform>());
        StartCoroutine(EnemySpawning());
    }

    IEnumerator EnemySpawning()
    {
        for (int i = 0; i < 3; ++i)
        {
            yield return StartCoroutine(SpawnWave(1f));

            yield return new WaitForSeconds(6f);
        }

        int waveCount = 1;
        while (true)
        {
            ++waveCount;
            yield return StartCoroutine(SpawnWave(waveCount));

            if (!TowerManager.instance.towersActive)
            {
                yield break;
            }
        }
    }

    IEnumerator SpawnWave(float healthMultiplier = 1f)
    {
        Debug.Log($"Spawning wave with multiplier {healthMultiplier}");

        for (int j = 0; j < 5; ++j)
        {
            SpawnEnemy(enemy1Prefab, healthMultiplier);

            yield return new WaitForSeconds(3);
        }

        SpawnEnemy(enemy2Prefab, healthMultiplier);

        yield return new WaitForSeconds(3);

        SpawnEnemy(enemy2Prefab, healthMultiplier);

        yield return new WaitForSeconds(3);

        SpawnEnemy(enemy2Prefab, healthMultiplier);

        yield return new WaitForSeconds(3);

        SpawnEnemy(enemy3Prefab, healthMultiplier);

        yield return new WaitForSeconds(3);
    }

    void SpawnEnemy(GameObject enemyPrefab, float healthMultiplier = 1f)
    {
        GameObject enemyObject = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
        Enemy enemy = enemyObject.GetComponent<Enemy>();
        enemy.healthMultiplier = healthMultiplier;
        enemies.Add(enemy);
    }

    public Enemy GetClosestEnemy(Vector3 position)
    {
        float smallestDistance = 999999999999f;

        Enemy closestEnemy = null;

        foreach (Enemy enemy in enemies)
        {
            float distance = Vector3.Distance(enemy.transform.position, position);

            if (distance < smallestDistance)
            {
                smallestDistance = distance;
                closestEnemy = enemy;
            }
        }

        return closestEnemy;
    }

    public void RemoveEnemy(Enemy enemy)
    {
        enemies.Remove(enemy);
    }
}
