using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerGenericUpgrade : MonoBehaviour
{
    protected Tower tower;

    [SerializeField] protected int baseCost;

    private void Awake()
    {
        tower = GetComponent<Tower>();
    }

    public bool CanEnable()
    {
        return ScoreManager.instance.playerResources >= baseCost;
    }
}
