using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDoubleBurst : TowerGenericUpgrade
{
    [SerializeField] GameObject newProjectile;

    private void OnEnable()
    {
        tower.SetProjectilePrefab(newProjectile);

        if (ScoreManager.instance.playerResources >= baseCost)
        {
            ScoreManager.instance.ChangePlayerResources(-baseCost);
        }
        else
        {
            this.enabled = false;
        }
    }

    private void OnDisable()
    {
        tower.SetProjectilePrefab(null);
    }
}
