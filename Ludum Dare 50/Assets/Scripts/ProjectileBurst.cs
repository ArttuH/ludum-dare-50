using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBurst : MonoBehaviour
{
    [SerializeField] GameObject shrapnelProjectile;
    [SerializeField] int shrapnelCount;

    public void Burst(
        Vector3 initialDirection,
        Vector3 firstLimitDirection,
        Vector3 lastLimitDirection,
        int multiplier)
    {
        Vector3 shrapnelMiddleDirection = -initialDirection;

        float rotateAngle = (Mathf.PI) / ((shrapnelCount * multiplier) + 2);

        Vector3 shrapnelDirection = Vector3.RotateTowards(firstLimitDirection, shrapnelMiddleDirection, rotateAngle, 0.0f);

        for (int shrapnelIndex = 0; shrapnelIndex < shrapnelCount; ++shrapnelIndex)
        {
            //Debug.DrawRay(transform.position, shrapnelDirection * 5f, Color.blue);

            GameObject projectile = Instantiate(shrapnelProjectile, transform.position, Quaternion.identity);
            projectile.transform.LookAt(transform.position + shrapnelDirection);

            shrapnelDirection = Vector3.RotateTowards(shrapnelDirection, lastLimitDirection, rotateAngle, 0.0f);
        }
    }
}
