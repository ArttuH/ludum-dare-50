using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class ClickerScript : MonoBehaviour
{
    [SerializeField] private AudioClip clickClip;

    [SerializeField] private float downPitch;
    [SerializeField] private float upPitch;

    [SerializeField] private AudioSource audioSource;

    PlayerControls playerControls;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();

        playerControls = new PlayerControls();
        playerControls.Menu.Click.performed += MouseButtonDown;
        playerControls.Menu.Click.canceled += MouseButtonUp;
    }

    private void OnEnable()
    {
        playerControls.Enable();
    }

    private void OnDisable()
    {
        playerControls.Disable();
    }

    private void Update()
    {
        /*if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                audioSource.pitch = downPitch;
                audioSource.PlayOneShot(clickClip);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                audioSource.pitch = upPitch;
                audioSource.PlayOneShot(clickClip);
            }
        }*/
    }

    void MouseButtonDown(InputAction.CallbackContext callbackContext)
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            audioSource.pitch = downPitch;
            audioSource.PlayOneShot(clickClip);
        }
    }

    void MouseButtonUp(InputAction.CallbackContext callbackContext)
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            audioSource.pitch = upPitch;
            audioSource.PlayOneShot(clickClip);
        }
    }
}
