using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;

    [SerializeField] GameObject musicPrefab;

    GameObject musicObject;
    AudioSource audioSource;

    [SerializeField] float normalVolume;
    [SerializeField] float fadeTime;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        GameObject oldMusicObject = GameObject.Find("Music(Clone)");

        if (oldMusicObject)
        {
            Destroy(oldMusicObject);
        }

        musicObject = Instantiate(musicPrefab);
        DontDestroyOnLoad(musicObject);

        audioSource = musicObject.GetComponent<AudioSource>();

        StartCoroutine(FadeIn());
    }

    public void StartFadeOut()
    {
        MusicFadeOut musicFadeOut = musicObject.GetComponent<MusicFadeOut>();
        musicFadeOut.StartFadeOut(fadeTime);
    }

    IEnumerator FadeIn()
    {
        float timer = 0;

        audioSource.volume = 0f;

        while (timer < fadeTime)
        {
            audioSource.volume = (timer / fadeTime) * normalVolume;
            timer += Time.deltaTime;
            yield return null;
        }

        audioSource.volume = normalVolume;
    }
}
