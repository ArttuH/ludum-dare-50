using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;


public class PlayerScript : MonoBehaviour
{
    public static PlayerScript instance;

    [SerializeField] Transform rangeDisplay;
    RangeDisplay rangeDisplayScript;
    MeshRenderer rangeDisplayMeshRenderer;

    [SerializeField] RectTransform towerCost;
    [SerializeField] private TextMeshProUGUI towerCostText;
    [SerializeField] Color towerCostGreen;
    [SerializeField] Color towerCostYellow;
    [SerializeField] Color towerCostRed;
    [SerializeField] Color towerCostColor;

    private GameObject hoveredTowerObject;

    PlayerControls playerControls;

    private Camera mainCamera;

    private bool groundHoverIsValid = false;
    private Vector3 hoverPosition; 

    private void Awake()
    {
        instance = this;

        mainCamera = Camera.main;

        playerControls = new PlayerControls();
        playerControls.Gameplay.Select.performed   += MouseButtonDown;
        playerControls.Gameplay.Select.canceled    += MouseButtonUp;
        playerControls.Gameplay.Deselect.performed += MouseButton2Down;
        playerControls.Gameplay.Deselect.canceled  += MouseButton2Up;

        //rangeDisplay.gameObject.SetActive(false);
        rangeDisplayScript = rangeDisplay.GetComponent<RangeDisplay>();
        rangeDisplayMeshRenderer = rangeDisplay.GetComponent<MeshRenderer>();
        rangeDisplayMeshRenderer.enabled = false;
    }

    private void OnEnable()
    {
        EnableControls();
    }

    private void OnDisable()
    {
        DisableControls();
    }

    public void EnableControls()
    {
        playerControls.Enable();
    }

    public void DisableControls()
    {
        playerControls.Disable();
    }

    // Update is called once per frame
    void Update()
    {
        Hover();
    }

    void Hover()
    {
        groundHoverIsValid = false;
        hoveredTowerObject = null;
        towerCost.gameObject.SetActive(false);
        rangeDisplayMeshRenderer.enabled = false;

        //rangeDisplay.gameObject.SetActive(false);

        Vector2 mousePosition = Mouse.current.position.ReadValue();
        Vector3 worldPoint = mainCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, mainCamera.nearClipPlane));

        // Raycast to ground
        Vector3 rayDirection = worldPoint - mainCamera.transform.position;
        RaycastHit raycastHit;
        Debug.DrawRay(worldPoint, rayDirection * 100f, Color.red);
        //if (Physics.Raycast(worldPoint, rayDirection, out raycastHit, 100f, layerMask))
        if (Physics.Raycast(worldPoint, rayDirection, out raycastHit, 100f))
        {
            if (raycastHit.collider.gameObject.CompareTag("Ground"))
            {
                GroundHover(raycastHit);
            }
            else if (raycastHit.collider.gameObject.CompareTag("Tower"))
            {
                TowerHover(raycastHit);
            }


        }
        else
        {
            //groundHoverIsValid = false;
            //rangeDisplay.gameObject.SetActive(false);
            rangeDisplayMeshRenderer.enabled = false;
        }
    }

    void GroundHover(RaycastHit raycastHit)
    {
        Vector3 groundPoint = raycastHit.point;

        groundPoint.x = Mathf.Round(groundPoint.x);
        groundPoint.y = 0;
        groundPoint.z = Mathf.Round(groundPoint.z);

        hoverPosition = groundPoint;
        groundHoverIsValid = true;

        if (ShopScript.instance.activeTowerPrefab != null)
        {
            if (TowerManager.instance.ValidTowerPlacement(groundPoint))
            {
                towerCost.gameObject.SetActive(true);
                UpdateTowerCost();
                ShowRangeDisplay(ShopScript.instance.activeTowerPrefab, true);
            }
            else
            {
                //rangeDisplay.gameObject.SetActive(false);
                rangeDisplayMeshRenderer.enabled = false;
            }
        }
    }

    void TowerHover(RaycastHit raycastHit)
    {
        hoveredTowerObject = raycastHit.collider.gameObject.transform.parent.gameObject;

        ShowRangeDisplay(hoveredTowerObject, false);
    }

    void MouseButtonDown(InputAction.CallbackContext callbackContext)
    {
        
    }

    void MouseButtonUp(InputAction.CallbackContext callbackContext)
    {
        if (groundHoverIsValid && ShopScript.instance.activeTowerPrefab != null)
        {
            CreateTower(ShopScript.instance.activeTowerPrefab, hoverPosition);
        }
        else if (hoveredTowerObject && ShopScript.instance.activeUpgradeType != UpgradeType.None)
        {
            switch (ShopScript.instance.activeUpgradeType)
            {
                case UpgradeType.DoublePower:
                    TowerDoublePower doublePower = hoveredTowerObject.GetComponent<TowerDoublePower>();
                    if (!doublePower.enabled)
                    {
                        doublePower.enabled = true;
                    }
                    break;

                case UpgradeType.DoubleRange:
                    TowerDoubleRange doubleRange = hoveredTowerObject.GetComponent<TowerDoubleRange>();
                    if (!doubleRange.enabled)
                    {
                        doubleRange.enabled = true;
                    }
                    break;

                default:
                    break;
            }
        }
    }

    void MouseButton2Down(InputAction.CallbackContext callbackContext)
    {
        ShopScript.instance.SetActiveTowerPrefab("0");
        ShopScript.instance.SetActiveUpgradeType("0");
    }

    void MouseButton2Up(InputAction.CallbackContext callbackContext)
    {

    }

    void CreateTower(GameObject tower, Vector3 towerPosition)
    {
        TowerManager.instance.CreateTower(tower, towerPosition, rangeDisplayScript.towerCount);
    }

    void ShowRangeDisplay(GameObject towerObject, bool useHoverPosition)
    {
        Tower tower = towerObject.GetComponent<Tower>();

        rangeDisplay.gameObject.SetActive(true);
        rangeDisplayMeshRenderer.enabled = true;

        float range = tower.GetRange() * 2;

        if (useHoverPosition)
        {
            rangeDisplay.position = hoverPosition;
        }
        else
        {
            rangeDisplay.position = towerObject.transform.position;
        }
        
        rangeDisplay.localScale = new Vector3(range, rangeDisplay.localScale.y, range);
    }

    void UpdateTowerCost()
    {
        int cost = CalculateActiveTowerCost();

        towerCostText.text = cost.ToString();
        towerCostText.color = towerCostColor;

        Vector2 viewportPosition = mainCamera.WorldToViewportPoint(hoverPosition);

        RectTransform canvasRect = EnemyManager.instance.canvasRect;
        Vector2 canvasPosition = new Vector2(
            ((viewportPosition.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
            ((viewportPosition.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));

        towerCost.anchoredPosition = canvasPosition;
    }

    int CalculateActiveTowerCost()
    {
        Tower towerTemplate = ShopScript.instance.activeTowerPrefab.GetComponent<Tower>();
        int totalCost = towerTemplate.baseCost + (100 * rangeDisplayScript.towerCount);

        if (ScoreManager.instance.playerResources >= totalCost)
        {
            if (rangeDisplayScript.towerCount == 0)
            {
                towerCostColor = towerCostGreen;
            }
            else
            {
                towerCostColor = towerCostYellow;
            }
        }
        else
        {
            towerCostColor = towerCostRed;
        }

        return totalCost;
    }
}
