using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShield : MonoBehaviour
{
    [SerializeField] GameObject shieldObject;

    [SerializeField] float shieldCooldownDuration;
    [SerializeField] float shieldCooldownTimer;

    public bool shieldActive;

    AudioSource audioSource;
    [SerializeField] AudioClip shieldDown;
    [SerializeField] AudioClip shieldUp;

    private void Awake()
    {
        shieldCooldownTimer = 0;
        shieldActive = false;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateShield();
    }

    void UpdateShield()
    {
        if (!shieldActive)
        {
            shieldCooldownTimer -= Time.deltaTime;

            if (shieldCooldownTimer <= 0)
            {
                ActivateShield();
            }
        }
    }

    public void ActivateShield()
    {
        shieldActive = true;
        shieldObject.SetActive(true);
        audioSource.PlayOneShot(shieldUp);
    }

    public void DeactivateShield()
    {
        shieldActive = false;
        shieldCooldownTimer = shieldCooldownDuration;
        shieldObject.SetActive(false);
        audioSource.PlayOneShot(shieldDown);
    }
}
