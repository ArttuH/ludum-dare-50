using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class PauseMenuScript : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenu;

    [SerializeField] private bool isPaused;

    PlayerControls playerControls;

    [SerializeField] PlayerScript playerScript;

    private void Awake()
    {
        playerScript = PlayerScript.instance;
        pauseMenu.SetActive(false);
        isPaused = false;
        playerControls = new PlayerControls();
        playerControls.Gameplay.Pause.performed += TogglePause;
    }

    private void OnEnable()
    {
        playerControls.Enable();
    }

    private void OnDisable()
    {
        playerControls.Disable();
    }

    void TogglePause(InputAction.CallbackContext callbackContext)
    {
        if (!isPaused)
        {
            PauseButtonPressed();
        }
        else
        {
            ContinueButtonPressed();
        }
    }

    public void PauseButtonPressed()
    {
        UIScript.instance.DisableGameUI();

        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        isPaused = true;

        playerScript.DisableControls();
    }

    public void ContinueButtonPressed()
    {
        UIScript.instance.EnableGameUI();

        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        isPaused = false;

        playerScript.EnableControls();
    }

    public void RestartButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("GameScene");
    }

    public void MainMenuButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitButtonPressed()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Application.Quit();
        }
    }
}
