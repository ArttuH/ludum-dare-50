using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject healthBarPrefab;
    EnemyHealthBar healthBar;
    RectTransform healthBarTransform;

    [SerializeField] GameObject destroyedEffect;

    Vector3[] path;

    int waypointIndex = 0;

    [SerializeField] float travelSpeed = 0.5f;
    [SerializeField] float health = 10.0f;

    float initialHealth;

    public int reward;
    public float healthMultiplier = 1f;

    Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;

        health = health * healthMultiplier;
        initialHealth = health;
        path = EnemyPath.instance.path;

        CreateHealthBar();

        StartCoroutine(Travel());
    }

    IEnumerator Travel()
    {
        transform.position = path[0];
        
        while (waypointIndex < path.Length-1)
        {
            Vector3 nextWaypoint = path[waypointIndex + 1];

            float initialDistance = Vector3.Distance(nextWaypoint, transform.position);
            float traveledDistance = 0;

            while (traveledDistance < initialDistance)
            {
                float movement = travelSpeed * Time.deltaTime;
                transform.position += (nextWaypoint - transform.position).normalized * movement;
                traveledDistance += movement;

                UpdateHealthBarPosition();

                yield return null;
            }

            transform.position = nextWaypoint;

            ++waypointIndex;
        }

        //Debug.Log($"Enemy {gameObject.name} reached the end!");
        ScoreManager.instance.GameOver();
    }

    public bool Hit(float damage)
    {
        health -= damage;

        healthBar.SetHealthBar(health / initialHealth);

        if (health <= 0)
        {
            Die();
            return true;
        }
        else
        {
            return false;
        }
    }

    void CreateHealthBar()
    {
        GameObject healthBarObject = Instantiate(healthBarPrefab, UIScript.instance.transform);
        healthBar = healthBarObject.GetComponent<EnemyHealthBar>();
        healthBarTransform = healthBarObject.GetComponent<RectTransform>();
        healthBarTransform.SetParent(EnemyManager.instance.hudTransform, true);
    }

    void UpdateHealthBarPosition()
    {
        Vector3 screenPosition = mainCamera.WorldToScreenPoint(transform.position);
        Vector2 viewportPosition = mainCamera.WorldToViewportPoint(transform.position);

        RectTransform canvasRect = EnemyManager.instance.canvasRect;
        Vector2 canvasPosition = new Vector2(
            ((viewportPosition.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
            ((viewportPosition.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));

        healthBarTransform.anchoredPosition = canvasPosition;

        //Debug.Log("UpdateHealthBarPosition()");
        //Debug.Log(healthBar.transform.position);
        //Debug.Log(screenPosition);
        //Debug.Log(healthBar.transform.position);
    }

    private void Die()
    {
        ScoreManager.instance.ChangePlayerResources(reward);
        ScoreManager.instance.enemiesDestroyed++;
        EnemyManager.instance.RemoveEnemy(this);
        healthBar.Destroy();

        if (destroyedEffect)
        {
            GameObject effect = Instantiate(destroyedEffect, transform.position, Quaternion.identity);
            Destroy(effect, 5f);
        }

        Destroy(gameObject);
    }
}
