using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDoubleRange : MonoBehaviour
{
    Tower tower;

    [SerializeField] int baseCost;

    [SerializeField] LineRenderer lineRenderer;

    private void Awake()
    {
        tower = GetComponent<Tower>();
    }

    private void OnEnable()
    {
        if (ScoreManager.instance.playerResources >= baseCost)
        {
            tower.SetRangeMultiplier(2f);
            ScoreManager.instance.ChangePlayerResources(-baseCost);
            TowerManager.instance.PlayTowerUpgradedSound();
            if (lineRenderer)
            {
                lineRenderer.enabled = true;
            }
        }
        else
        {
            this.enabled = false;
        }
    }

    private void OnDisable()
    {
        tower.SetRangeMultiplier(1f);
    }
}
