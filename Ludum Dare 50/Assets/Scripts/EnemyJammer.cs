using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyJammer : MonoBehaviour
{
    List<Tower> nearbyTowers;

    Tower jamTarget;

    LineRenderer lineRenderer;

    [SerializeField] Transform jammerOrigin;
    [SerializeField] Vector3 jammerTargetOffset;

    AudioSource audioSource;
    [SerializeField] AudioClip audioClip;

    private void Awake()
    {
        nearbyTowers = new List<Tower>();
        jamTarget = null;

        lineRenderer = GetComponent<LineRenderer>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        Jam();
    }

    private void OnTriggerEnter(Collider other)
    {
        Transform possibleTower = other.transform.parent;

        if (possibleTower)
        {
            Tower tower = possibleTower.GetComponent<Tower>();

            if (tower)
            {
                if (nearbyTowers.Count == 0)
                {
                    StartJam(tower);
                }

                nearbyTowers.Add(tower);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Transform possibleTower = other.transform.parent;

        if (possibleTower)
        {
            Tower tower = possibleTower.GetComponent<Tower>();

            if (tower)
            {
                if (tower == jamTarget || possibleTower == jamTarget.transform)
                {
                    EndJam();
                    nearbyTowers.Remove(tower);
                    FindNewJamTarget();
                }

                //nearbyTowers.Remove(tower);
                //FindNewJamTarget();
            }
        }
    }

    void StartJam(Tower tower)
    {
        jamTarget = tower;
        jamTarget.enabled = false;
        lineRenderer.enabled = true;

        audioSource.Stop();
        audioSource.PlayOneShot(audioClip);
    }

    void EndJam()
    {
        if (jamTarget)
        {
            jamTarget.enabled = true;
        }
        
        lineRenderer.enabled = false;
    }

    void FindNewJamTarget()
    {
        float smallestDistance = 9999999f;

        foreach (Tower tower in nearbyTowers)
        {
            float distance = Vector3.Distance(transform.position, tower.transform.position);

            if (distance < smallestDistance)
            {
                smallestDistance = distance;
                jamTarget = tower;
            }
        }

        if (jamTarget)
        {
            StartJam(jamTarget);
        }
    }

    void Jam()
    {
        if (jamTarget == null)
        {
            return;
        }

        lineRenderer.SetPosition(0, jammerOrigin.position);
        lineRenderer.SetPosition(1, jamTarget.transform.position + jammerTargetOffset);
    }

    private void OnDestroy()
    {
        EndJam();
    }
}
