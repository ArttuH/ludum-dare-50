using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDoubleDamage : MonoBehaviour
{
    Tower tower;

    [SerializeField] int baseCost;

    private void Awake()
    {
        tower = GetComponent<Tower>();
    }

    private void OnEnable()
    {
        tower.SetDamageMultiplier(2f);

        if (ScoreManager.instance.playerResources >= baseCost)
        {
            ScoreManager.instance.ChangePlayerResources(-baseCost);
        }
        else
        {
            this.enabled = false;
        }
    }

    private void OnDisable()
    {
        tower.SetDamageMultiplier(1f);
    }
}
