using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDoubleFirerate : TowerGenericUpgrade
{
    private void OnEnable()
    {
        if (ScoreManager.instance.playerResources >= baseCost)
        {
            tower.SetFirerateMultiplier(2f);
            ScoreManager.instance.ChangePlayerResources(-baseCost);
        }
        else
        {
            this.enabled = false;
        }
    }

    private void OnDisable()
    {
        tower.SetFirerateMultiplier(1f);
    }
}
