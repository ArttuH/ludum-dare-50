using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TowerManager : MonoBehaviour
{
    public static TowerManager instance;

    Dictionary<Vector2Int, Tower> towers;

    AudioSource audioSource;

    [SerializeField] AudioClip[] towerBuiltAudioClips;
    [SerializeField] AudioClip towerUpgradedAudioClip;
    [SerializeField] float audioMinPitch;
    [SerializeField] float audioMaxPitch;

    public bool towersActive;

    private void Awake()
    {
        instance = this;
        towers = new Dictionary<Vector2Int, Tower>();
        audioSource = GetComponent<AudioSource>();
        towersActive = true;
    }

    public bool ValidTowerPlacement(Vector3 towerPosition)
    {
        Vector2Int towerKey = new Vector2Int(Mathf.RoundToInt(towerPosition.x), Mathf.RoundToInt(towerPosition.z));

        return ValidTowerPlacement(towerKey);
    }

    public bool ValidTowerPlacement(Vector2Int towerKey)
    {
        bool blockedByPath = false;
        EnemyPath.instance.pathPositions.TryGetValue(towerKey, out blockedByPath);
        if (blockedByPath)
            return false;

        Tower tower;
        towers.TryGetValue(towerKey, out tower);
        return (tower == null);
    }

    public void CreateTower(GameObject towerPrefab, Vector3 towerPosition, int nearbyTowerCount)
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        Vector2Int towerKey = new Vector2Int(Mathf.RoundToInt(towerPosition.x), Mathf.RoundToInt(towerPosition.z));

        if (!ValidTowerPlacement(towerKey))
        {
            Debug.Log("Not a valid tower placement!");
            return;
        }

        Tower towerTemplate = towerPrefab.GetComponent<Tower>();
        int totalCost = towerTemplate.baseCost + (100 * nearbyTowerCount);

        if (ScoreManager.instance.playerResources < totalCost)
        {
            return;
        }

        ScoreManager.instance.ChangePlayerResources(-totalCost);

        GameObject towerObject = Instantiate(towerPrefab, towerPosition, Quaternion.identity);
        Tower tower = towerObject.GetComponent<Tower>();
        towers.Add(towerKey, tower);

        PlayTowerBuiltSound();
    }

    public void PlayTowerBuiltSound()
    {
        audioSource.pitch = Random.Range(audioMinPitch, audioMaxPitch);
        audioSource.PlayOneShot(towerBuiltAudioClips[Random.Range(0, towerBuiltAudioClips.Length)]);
    }

    public void PlayTowerUpgradedSound()
    {
        audioSource.pitch = Random.Range(audioMinPitch, audioMaxPitch);
        audioSource.PlayOneShot(towerUpgradedAudioClip);
    }
}
