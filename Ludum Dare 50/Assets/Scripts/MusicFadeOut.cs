using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicFadeOut : MonoBehaviour
{
    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void StartFadeOut(float fadeTime)
    {
        StartCoroutine(FadeOut(fadeTime));
    }

    IEnumerator FadeOut(float fadeTime)
    {
        float timer = 0;

        float startVolume = audioSource.volume;

        while (timer < fadeTime)
        {
            audioSource.volume = (1 - (timer / fadeTime)) * startVolume;
            timer += Time.deltaTime;
            yield return null;
        }

        audioSource.volume = 0f;
    }
}
