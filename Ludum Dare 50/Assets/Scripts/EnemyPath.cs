using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPath : MonoBehaviour
{
    [SerializeField] GameObject smallWaypoint;

    public Vector3[] path;

    public Dictionary<Vector2Int, bool> pathPositions;

    public static EnemyPath instance;

    private void Awake()
    {
        instance = this;

        UpdatePath();
    }

    private void UpdatePath()
    {
        pathPositions = new Dictionary<Vector2Int, bool>();

        Transform waypointParent = transform.GetChild(0);

        path = new Vector3[waypointParent.childCount];

        for (int waypointIndex = 0; waypointIndex < waypointParent.childCount; ++waypointIndex)
        {
            Vector3 position = waypointParent.GetChild(waypointIndex).transform.position;
            path[waypointIndex] = position;
        }

        for (int waypointIndex = 0; waypointIndex < path.Length - 1; ++waypointIndex)
        {
            Vector3 position = path[waypointIndex];
            
            Vector3 nextWaypointPosition = path[waypointIndex + 1];

            float distanceToNextWaypoint = Vector3.Distance(nextWaypointPosition, position);
            int steps = Mathf.RoundToInt(distanceToNextWaypoint);

            Vector3 direction = (nextWaypointPosition - position).normalized;

            for (int i = 0; i < steps; ++i)
            {
                Vector3 nextPosition = position + (direction * i);
                Vector2Int pathKey = new Vector2Int(Mathf.RoundToInt(nextPosition.x), Mathf.RoundToInt(nextPosition.z));
                pathPositions.Add(pathKey, true);
                CreateSmallWaypoint(nextPosition);
            }
        }

        Vector3 lastPosition = path[path.Length - 1];
        Vector2Int lastPathKey = new Vector2Int(Mathf.RoundToInt(lastPosition.x), Mathf.RoundToInt(lastPosition.z));
        pathPositions.Add(lastPathKey, true);
    }

    void CreateSmallWaypoint(Vector3 position)
    {
        Instantiate(smallWaypoint, position, Quaternion.identity);
    }
}
