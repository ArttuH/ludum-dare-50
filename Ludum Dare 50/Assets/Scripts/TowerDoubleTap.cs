using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDoubleTap : TowerGenericUpgrade
{
    private void OnEnable()
    {
        tower.SetShotTapCount(2);

        if (ScoreManager.instance.playerResources >= baseCost)
        {
            ScoreManager.instance.ChangePlayerResources(-baseCost);
        }
        else
        {
            this.enabled = false;
        }
    }

    private void OnDisable()
    {
        tower.SetShotTapCount(1);
    }
}
