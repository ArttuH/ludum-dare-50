using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    public int playerResources;

    public float timeElapsed;

    public bool isPaused;

    public bool gameOver;

    public int enemiesDestroyed;
    public int shotsFired;

    private void Awake()
    {
        instance = this;
        gameOver = false;
        isPaused = false;

        enemiesDestroyed = 0;
        shotsFired = 0;
    }

    private void Start()
    {
        UpdatePlayerResources();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimeElapsed();
    }

    void UpdatePlayerResources()
    {
        UIScript.instance.SetResourcesValue(playerResources);
    }

    public void ChangePlayerResources(int change)
    {
        playerResources += change;
        UpdatePlayerResources();
    }

    void UpdateTimeElapsed()
    {
        if (!isPaused && !gameOver)
        {
            timeElapsed += Time.deltaTime;
        }

        UIScript.instance.SetTimeElapsedValue(timeElapsed);
    }

    public void GameOver()
    {
        if (!gameOver)
        {
            gameOver = true;
            UIScript.instance.ShowEndScreen();
        }
    }
}
