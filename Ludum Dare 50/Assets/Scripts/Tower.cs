using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FireMode
{
    Single,
    Triple
}

public class Tower : MonoBehaviour
{
    [SerializeField] Transform turret;
    [SerializeField] Transform projectileSpawnPoint;
    [SerializeField] GameObject originalProjectilePrefab;
    [SerializeField] float range;
    [SerializeField] float damage;
    [SerializeField] float cooldownDuration;
    [SerializeField] float cooldownTimer;
    [SerializeField] Transform aimOffsetTarget;
    [SerializeField] FireMode fireMode;
    [SerializeField] Kickback kickback;

    public float rangeMultiplier;
    public float damageMultiplier;
    public float firerateMultiplier;
    public int   shotTapCount;
    public GameObject projectilePrefab;

    public int baseCost;

    Transform aimTarget = null;

    AudioSource audioSource;

    [SerializeField] AudioClip[] shotAudioClips;
    [SerializeField] float shotAudioMinPitch;
    [SerializeField] float shotAudioMaxPitch;

    private void Awake()
    {
        cooldownTimer = cooldownDuration / firerateMultiplier;
        rangeMultiplier = 1f;
        damageMultiplier = 1f;
        projectilePrefab = originalProjectilePrefab;

        audioSource = GetComponent<AudioSource>();
        kickback = GetComponent<Kickback>();
    }

    // Update is called once per frame
    void Update()
    {
        if (aimTarget && TowerManager.instance.towersActive)
        {
            if (Vector3.Distance(aimTarget.position, transform.position) > (range * rangeMultiplier))
            {
                aimTarget = null;
            }
            else
            {
                aimOffsetTarget.position = new Vector3(aimTarget.position.x, projectileSpawnPoint.position.y, aimTarget.position.z);
                turret.LookAt(aimOffsetTarget, Vector3.up);
                ShootLoop();
            }
        }
        else
        {
            if (EnemyManager.instance.enemies.Count > 0)
            {
                aimTarget = EnemyManager.instance.GetClosestEnemy(transform.position).transform;
            }
        }
    }

    void ShootLoop()
    {
        cooldownTimer -= Time.deltaTime;

        if (cooldownTimer <= 0)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        cooldownTimer = cooldownDuration / firerateMultiplier;

        StartCoroutine(ShotTap());
    }

    IEnumerator ShotTap()
    {
        for (int i = 0; i < shotTapCount; ++i)
        {
            ScoreManager.instance.shotsFired++;
            GameObject projectileObject = Instantiate(projectilePrefab, projectileSpawnPoint.position, turret.rotation);
            Projectile projectile = projectileObject.GetComponent<Projectile>();
            projectile.parentTower = this;
            projectile.damage = (damage * damageMultiplier);

            PlayShotAudio();

            if (kickback)
            {
                kickback.StartKick();
            }

            if (fireMode == FireMode.Triple)
            {
                float spreadAngle = 5f;

                yield return new WaitForSeconds(0.06f);

                ScoreManager.instance.shotsFired++;
                GameObject secondProjectileObject = Instantiate(projectilePrefab, projectileSpawnPoint.position, turret.rotation);
                secondProjectileObject.transform.Rotate(Vector3.up, spreadAngle, Space.World);
                Projectile secondProjectile = secondProjectileObject.GetComponent<Projectile>();
                secondProjectile.parentTower = this;
                secondProjectile.damage = (damage * damageMultiplier);

                yield return new WaitForSeconds(0.06f);

                ScoreManager.instance.shotsFired++;
                GameObject thirdProjectileObject = Instantiate(projectilePrefab, projectileSpawnPoint.position, turret.rotation);
                thirdProjectileObject.transform.Rotate(Vector3.up, -spreadAngle, Space.World);
                Projectile thirdProjectile = thirdProjectileObject.GetComponent<Projectile>();
                thirdProjectile.parentTower = this;
                thirdProjectile.damage = (damage * damageMultiplier);
            }

            yield return new WaitForSeconds(0.2f);
        }
    }

    public void ResetAimTarget()
    {
        aimTarget = null;
    }

    public void SetDamageMultiplier(float multiplier)
    {
        damageMultiplier = multiplier;
    }

    public void SetRangeMultiplier(float multiplier)
    {
        rangeMultiplier = multiplier;
    }

    public void SetFirerateMultiplier(float multiplier)
    {
        firerateMultiplier = multiplier;
    }

    public void SetShotTapCount(int count)
    {
        shotTapCount = count;
    }

    public void SetProjectilePrefab(GameObject prefab)
    {
        if (prefab)
        {
            projectilePrefab = prefab;
        }
        else
        {
            projectilePrefab = originalProjectilePrefab;
        }
    }

    public float GetRange()
    {
        return range * rangeMultiplier;
    }

    void PlayShotAudio()
    {
        audioSource.pitch = Random.Range(shotAudioMinPitch, shotAudioMaxPitch);
        audioSource.PlayOneShot(shotAudioClips[Random.Range(0, shotAudioClips.Length)]);
    }
}
