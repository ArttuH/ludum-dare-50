using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kickback : MonoBehaviour
{
    [SerializeField] AnimationCurve animationCurve;
    [SerializeField] float kickDuration;
    [SerializeField] float kickAmplitude;
    [SerializeField] Transform kickTarget;
    [SerializeField] Vector3 kickAxis;
    [SerializeField] bool kickX;
    [SerializeField] bool kickY;
    [SerializeField] bool kickZ;

    public void StartKick()
    {
        StartCoroutine(Kick());

        if (kickX)
        {
            kickAxis = kickTarget.transform.right;
        }
        else if (kickY)
        {
            kickAxis = kickTarget.transform.up;
        }
        else if (kickZ)
        {
            kickAxis = kickTarget.transform.forward;
        }
    }

    IEnumerator Kick()
    {
        float timer = 0;

        Vector3 startPosition = kickTarget.position;

        

        while (timer < kickDuration)
        {
            kickTarget.position = startPosition + (kickAxis * kickAmplitude * animationCurve.Evaluate(timer / kickDuration));

            timer += Time.deltaTime;
            yield return null;
        }
    }
}
