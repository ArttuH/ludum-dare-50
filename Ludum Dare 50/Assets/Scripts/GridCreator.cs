using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCreator : MonoBehaviour
{
    [SerializeField] int width  = 100;
    [SerializeField] int height = 100;
    [SerializeField] int widthSpacing  = 10;
    [SerializeField] int heightSpacing = 10;
    [SerializeField] GameObject gridLinePrefab;

    private void Awake()
    {
        CreateGrid();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateGrid()
    {
        for (int x = 0; x <= width; x += widthSpacing)
        {
            CreateVerticalLine(x);
        }

        for (int y = 0; y <= height; y += heightSpacing)
        {
            CreateHorizontalLine(y);
        }
    }

    void CreateHorizontalLine(int y)
    {
        //int x = -width / 2;
        int x = 0;
        CreateLine(x, y, Vector3.right);
    }

    void CreateVerticalLine(int x)
    {
        //int y = -height / 2;
        int y = 0;
        CreateLine(x, y, Vector3.forward);
    }

    void CreateLine(int x, int y, Vector3 facing)
    {
        Vector3 linePosition = new Vector3(x - (width / 2) - 0.5f, 0.01f, y - (height / 2) - 0.5f);
        Quaternion lineRotation = Quaternion.identity;
        lineRotation.SetLookRotation(facing, Vector3.up);
        GameObject Line = Instantiate(gridLinePrefab);
        Line.transform.position = linePosition;
        Line.transform.rotation = lineRotation;

        Line.transform.parent = transform;
    }
}
