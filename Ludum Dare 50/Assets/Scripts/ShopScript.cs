using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UpgradeType
{
    None,
    DoublePower,
    DoubleRange
}

public class ShopScript : MonoBehaviour
{
    public static ShopScript instance;

    public GameObject tower1Prefab;
    public GameObject tower2Prefab;
    public GameObject tower3Prefab;

    public GameObject activeTowerPrefab;
    public UpgradeType activeUpgradeType;

    [SerializeField] Transform shopTowerHighlight;
    [SerializeField] Transform shopTowerButton1;
    [SerializeField] Transform shopTowerButton2;
    [SerializeField] Transform shopTowerButton3;

    [SerializeField] Transform shopUpgradeHighlight;
    [SerializeField] Transform shopUpgradeButton1;
    [SerializeField] Transform shopUpgradeButton2;

    private void Awake()
    {
        instance = this;
        activeUpgradeType = UpgradeType.None;
    }

    public void SetActiveTowerPrefab(string towerNumber)
    {
        shopTowerHighlight.gameObject.SetActive(true);

        switch (towerNumber)
        {
            case "1":
                activeTowerPrefab = tower1Prefab;
                MoveShopTowerHighlight(shopTowerButton1);
                SetActiveUpgradeType("0");
                break;
            case "2":
                activeTowerPrefab = tower2Prefab;
                MoveShopTowerHighlight(shopTowerButton2);
                SetActiveUpgradeType("0");
                break;
            case "3":
                activeTowerPrefab = tower3Prefab;
                MoveShopTowerHighlight(shopTowerButton3);
                SetActiveUpgradeType("0");
                break;
            default:
                activeTowerPrefab = null;
                shopTowerHighlight.gameObject.SetActive(false);
                break;
        }
    }

    void MoveShopTowerHighlight(Transform buttonTransform)
    {
        shopTowerHighlight.transform.position = buttonTransform.position;
    }

    public void SetActiveUpgradeType(string upgradeNumber)
    {
        shopUpgradeHighlight.gameObject.SetActive(true);

        switch (upgradeNumber)
        {
            case "1":
                activeUpgradeType = UpgradeType.DoublePower;
                MoveShopUpgradeHighlight(shopUpgradeButton1);
                SetActiveTowerPrefab("0");
                break;
            case "2":
                activeUpgradeType = UpgradeType.DoubleRange;
                MoveShopUpgradeHighlight(shopUpgradeButton2);
                SetActiveTowerPrefab("0");
                break;
            default:
                activeUpgradeType = UpgradeType.None;
                shopUpgradeHighlight.gameObject.SetActive(false);
                break;
        }
    }

    void MoveShopUpgradeHighlight(Transform buttonTransform)
    {
        shopUpgradeHighlight.transform.position = buttonTransform.position;
    }
}
