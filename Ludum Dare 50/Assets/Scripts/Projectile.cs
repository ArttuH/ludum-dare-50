using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Tower parentTower;
    [SerializeField] float speed;
    public float damage;
    [SerializeField] float lifetime;

    ProjectileBurst projectileBurst;

    private int layerMask = 0;

    float radius;

    [SerializeField] GameObject audioSourcePrefab;
    [SerializeField] AudioClip[] impactAudioClips;
    [SerializeField] float impactAudioMinPitch;
    [SerializeField] float impactAudioMaxPitch;

    [SerializeField] GameObject impactPrefab;

    private void Awake()
    {
        layerMask = LayerMask.GetMask("Enemy", "Shield");

        radius = transform.GetChild(0).transform.localScale.x;

        projectileBurst = GetComponent<ProjectileBurst>();
    }

    private void Start()
    {
        Destroy(gameObject, lifetime);
    }

    // Update is called once per frame
    void Update()
    {
        float movement = speed * Time.deltaTime;

        RaycastHit raycastHit;
        if (Physics.SphereCast(transform.position, radius, transform.forward, out raycastHit, movement, layerMask))
        {
            Transform colliderParent = raycastHit.collider.gameObject.transform.parent;

            if (colliderParent)
            {
                EnemyShield shield = colliderParent.GetComponent<EnemyShield>();
                if (shield && shield.shieldActive)
                {
                    shield.DeactivateShield();
                }
                else
                {
                    Enemy enemy = raycastHit.collider.gameObject.transform.parent.GetComponent<Enemy>();

                    if (enemy)
                    {
                        bool enemyDied = enemy.Hit(damage);

                        if (enemyDied)
                        {
                            parentTower.ResetAimTarget();
                        }
                    }
                }
            }
            else
            {
                Debug.Log($"Projectile collided with {raycastHit.collider.name}");
            }

            if (projectileBurst)
            {
                projectileBurst.Burst(
                    transform.forward,
                    transform.right,
                    -transform.right,
                    1);
            }

            PlayImpactAudio();

            if (impactPrefab)
                Instantiate(impactPrefab, transform.position, Quaternion.identity);

            Destroy(gameObject);
        }
        else
        {
            transform.Translate(transform.forward * movement, Space.World);
        }
    }

    void PlayImpactAudio()
    {
        GameObject audioSourceObject = Instantiate(audioSourcePrefab, transform.position, Quaternion.identity);
        AudioSource audioSource = audioSourceObject.GetComponent<AudioSource>();
        audioSource.pitch = Random.Range(impactAudioMinPitch, impactAudioMaxPitch);
        audioSource.PlayOneShot(impactAudioClips[Random.Range(0, impactAudioClips.Length)]);
        Destroy(audioSourceObject, 1f);
    }
}
