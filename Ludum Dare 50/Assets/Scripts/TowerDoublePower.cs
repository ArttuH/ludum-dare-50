using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDoublePower : MonoBehaviour
{
    [SerializeField] TowerGenericUpgrade upgrade;
    [SerializeField] MeshRenderer meshRenderer;
    Material barrelMaterial;

    private void Awake()
    {
        upgrade = GetComponent<TowerGenericUpgrade>();
        barrelMaterial = meshRenderer.materials[meshRenderer.materials.Length - 1];
    }

    private void OnEnable()
    {
        if (upgrade && upgrade.CanEnable())
        {
            upgrade.enabled = true;
            TowerManager.instance.PlayTowerUpgradedSound();
            barrelMaterial.EnableKeyword("_EMISSION");
        }
        else
        {
            this.enabled = false;
        }
    }

    private void OnDisable()
    {
        if (upgrade)
        {
            upgrade.enabled = false;
        }
    }
}
