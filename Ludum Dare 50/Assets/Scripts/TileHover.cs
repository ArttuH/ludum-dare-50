using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TileHover : MonoBehaviour
{
    private Camera mainCamera;

    private int layerMask = 0;

    private void Awake()
    {
        mainCamera = Camera.main;
        layerMask = LayerMask.GetMask("Ground");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Hover();
    }

    void Hover()
    {
        //Event currentEvent = Event.current;

        //if (currentEvent == null)
        //    return;

        //Vector2 mousePosition = new Vector2(currentEvent.mousePosition.x, mainCamera.pixelHeight - currentEvent.mousePosition.y);
        Vector2 mousePosition = Mouse.current.position.ReadValue();
        Vector3 worldPoint = mainCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, mainCamera.nearClipPlane));

        // Raycast to ground
        Vector3 rayDirection = worldPoint - mainCamera.transform.position;
        RaycastHit raycastHit;
        Debug.DrawRay(worldPoint, rayDirection * 100f, Color.red);
        if (Physics.Raycast(worldPoint, rayDirection, out raycastHit, 100f, layerMask))
        {
            Vector3 groundPoint = raycastHit.point;

            groundPoint.x = Mathf.Round(groundPoint.x);
            groundPoint.y = 0;
            groundPoint.z = Mathf.Round(groundPoint.z);

            transform.position = groundPoint;
        }
    }
}
